<?php

/**
 * @file
 * Defines a field type that will integrate with the Aggregator module, adding
 * and removing feeds.
 *
 */

if (!function_exists('aggregation_sync_feed')) {
  // The aggregation API WAS shared with the feed_node 5.x module.  Not sure if
  // any 6.x modules use it?
  include_once 'aggregation_api.inc';
}

/**
 * Implementation of hook_theme().
 */
function feedfield_theme() {
  return array(
      'feedfield' => array(
        'arguments' => array('element' => NULL),
        ),
      'feedfield_formatter_default' => array(
        'arguments' => array('element' => NULL),
        ),
      );
}

/**
 * Implementation of hook_field_info().
 *
 * Here we indicate that the content module will use its default
 * handling for the view of this field.
 *
 * Callbacks can be omitted if default handing is used.
 * They're included here just so this module can be used
 * as an example for custom modules that might do things
 * differently.
 */
function feedfield_field_info() {
  return array(
      'feedfield' => array(
        'label' => t('Feed'),
        'description' => t('Create aggregator feed'),
        'callbacks' => array(
          'tables' => CONTENT_CALLBACK_DEFAULT,
          'arguments' => CONTENT_CALLBACK_DEFAULT,
          ),
        ),
      );
}

/**
 * Implementation of hook_field_settings().
 */
function feedfield_field_settings($op, $field) {
  switch ($op) {
    case 'form':
      $form = array();
      $form['categories_fieldset'] = array(
        '#type' => 'fieldset',
        '#title' => t('Force Categories'),
        '#collapsible' => TRUE,
        '#collapsed' => empty($field['force_categories'])
      );

      $form['categories_fieldset']['force_categories'] = array(
        '#type' => 'checkbox',
        '#title' => t('Force Feed Categories'),
        '#default_value' => $field['force_categories'],
        '#description' => t('If checked, users will not have the option to
                             select the feed\'s categories, and it will automatically be put in the
                             category chosen below.'),
      );

      $form['categories_fieldset']['categories'] = array(
        '#type' => 'checkboxes',
        '#options' => aggregation_get_all_feed_categories(),
        '#default_value' => !empty($field['categories']) ?
        $field['categories'] : array(NULL)
      );
      $form['refresh_fieldset'] = array(
        '#type' => 'fieldset',
        '#title' => t('Force Refesh Interval'),
        '#collapsible' => TRUE,
        '#collapsed' => empty($field['force_refresh'])
      );

      $form['refresh_fieldset']['force_refresh'] = array(
        '#type' => 'checkbox',
        '#title' => t('Force Feed Refresh Interval'),
        '#default_value' => $field['force_refresh'],
        '#description' => t('If checked, users will not have the option to
                             select the feed\'s refresh interval, and it will
                             automatically be set to the value chosen below.'),
      );

      $form['refresh_fieldset']['refresh'] = array(
        '#type' => 'select',
        '#options' => aggregation_get_allowed_refresh_intervals(),
        '#default_value' => isset($field['refresh']) ?
        $field['refresh'] : 3600
      );

      $form['force_title'] = array(
        '#type' => 'checkbox',
        '#title' => t('Force Feed Title To Node Title'),
        '#default_value' => $field['force_title'],
        '#description' => t('If checked, users will not have the option to
                             select the feed\'s title, and it will
                             automatically be set to the node\'s title.')
      );
      return $form;

    case 'validate':
      if ($field['multiple'] != 0 && !empty($field['force_title'])) {
        form_set_error('force_title', t('Cannot force title on multiple
                        values'));
      }
      break;

    case 'save':
      return array('force_categories', 'categories', 'force_refresh',
      'refresh', 'force_title');

    case 'database columns':
      return array(
          /* Only need to store fid, everything else will be stored by Aggregator.
           * content_type_* will only serve as a junction table to link feeds
           * to node versions.
           */
          'fid' => array('type' => 'int'),
          );

  }
}

/**
 * Implementation of hook_field().
 */
function feedfield_field($op, &$node, $field, &$node_field, $teaser, $page) {
  switch ($op) {

    case 'presave':
      foreach ($node_field as $delta => &$item) {
        if(!empty($field['force_title'])) {
          $item['title'] = $node->title;
        }
      }
      break;

    case 'load':
      if ($field['type'] == 'feedfield') {
        return _add_values_on_load($node_field, $field);
      }
      break;

    case 'validate':
      foreach ($node_field as $delta => $value) {
        _feedfield_validate($node_field[$delta], $delta, $field, $node);
      }
      break;

    case 'insert':
      if ($field['type'] == 'feedfield') {
        foreach ($node_field as $delta => &$item) {
          if (!empty($item['feed'])) {
            $new_fid = aggregation_sync_feed($item['title'], $item['feed']);
            // we have a feed url
            if ($new_fid != -1) {
              aggregation_set_refresh_interval($new_fid, $item['refresh']);
              if (is_array($item['categories'])) {
                _set_categories($item['categories'], $new_fid);
              }
              /* If we have a new feed ID, then we are going to need to remove
               * the old feed
               */
              if (!empty($item['fid']) && $item['fid'] != $new_fid) {
                _remove_feed($item['fid']);
              }
            }
            // and now set the variable to have cck save it to the database.
            $item['fid'] = $new_fid;
          } elseif ($item['fid']) {
            // no feed, but a feed id, that means we have to remove this feed
            _remove_feed($item['fid']);
          }
        }
      }
      break;

    case 'update':
      // Delete and insert, rather than update, in case a field was added.
      if ($field['type'] == 'feedfield') {

        feedfield_field('insert', $node, $field, $node_field, $teaser, $page);
      }
      break;

    case 'delete':
      if ($field['type'] == 'feedfield') {
        // remove all linked feeds
        foreach ($node_field as $delta => $item) {
          if ($item['value']['fid']) {
            _remove_feed($item['value']['fid']);
          }
        }
      }
      break;

  }
}

/**
 * Implementation of hook_content_is_empty().
 */
function feedfield_content_is_empty($item, $field) {
  if (empty($item['feed'])) {
    return TRUE;
  }
  return FALSE;
}


/**
 * Implementation of hook_widget_info().
 */
function feedfield_widget_info() {
  return array(
      'feedfield' => array(
        'label' => 'Aggregator Feed',
        'field types' => array('feedfield'),
        'multiple values' => CONTENT_HANDLE_CORE,
        'callbacks' => array(
          'default value' => CONTENT_CALLBACK_DEFAULT,
          ),
        ),
      );
}

/**
 * Implementation of hook_widget().
 */
function feedfield_widget(&$form, &$form_state, $field, $items, $delta = 0) {

  $element = array(
      '#type' => $field['widget']['type'],
      '#default_value' => isset($items[$delta]) ? $items[$delta] : '',
      );
  return $element;
}

/**
 * Implementation of FAPI hook_elements().
 */
function feedfield_elements() {
  return array(
      'feedfield' => array(
        '#input' => TRUE,
        '#columns' => array('title', 'feed', 'categories', 'refresh', 'fid'),
        '#process' => array('feedfield_process'),
        ),
      );
}

/**
 * Process the link type element before displaying the field.
 */
function feedfield_process($element, $edit, $form_state, $form) {
  $field = $form['#field_info'][$element['#field_name']];
  $delta = $element['#delta'];

  if(empty($field['force_title'])) {
    $element['title'] = array(
        '#type' => 'textfield',
        '#maxlength' => '255',
        '#title' => t('Feed title'),
        '#default_value' => isset($element['#value']['value']['title']) ?
        $element['#value']['value']['title'] : $element['#value']['title']
        );
  } else {
    $element['title'] = array(
        '#type' => 'value',
        '#value' => isset($element['#value']['value']['fid']) ?
        $element['#value']['value']['fid'] : NULL
        );
  }

  $element['feed'] = array(
      '#type' => 'textfield',
      '#maxlength' => '255',
      '#title' => t(' Feed URL'),
      '#default_value' => isset($element['#value']['value']['feed']) ?
      $element['#value']['value']['feed'] : $element['#value']['feed'],
      '#required' => ($delta == 0) ? $field['required'] : FALSE,
      );

  if(empty($field['force_refresh'])) {
    $element['refresh'] = array(
        '#type' => 'select',
        '#title' => t('Update interval'),
        '#default_value' => isset($element['#value']['value']['refresh']) ?
        $element['#value']['value']['refresh'] :
        (isset($element['#value']['refresh']) ? $element['#value']['refresh'] :
         3600),
        '#options' => aggregation_get_allowed_refresh_intervals()
        );
  } else {
    $element['refresh'] = array(
        '#type' => 'value',
        '#value' => $field['refresh']
        );
  }

  $all_feed_categories = aggregation_get_all_feed_categories();
  // If there are no categories then do not display form item.
  if (empty($all_feed_categories)) {
    $element['categories'] = array(
        '#type' => 'value',
        '#value' => array(NULL)
        );
  } else if(empty($field['force_categories'])) {
    $element['categories'] = array(
        '#type' => 'checkboxes',
        '#title' => t('Aggregator Categories'),
        '#options' => $all_feed_categories,
        '#default_value' => !empty($element['#value']['value']['categories']) ?
        $element['#value']['value']['categories'] : 
        (is_array($element['#value']['categories']) ? $element['#value']['categories'] :
         array(NULL))
        );
  } else {
    // Categories are forced to field settings.
    $element['categories'] = array(
        '#type' => 'value',
        '#value' => $field['categories']
        );
  }

  $element['fid'] = array(
      '#type' => 'hidden',
      '#default_value' => !empty($element['#value']['value']['fid']) ?
      $element['#value']['value']['fid'] : NULL
      );

  return $element;

} 
/**
 * Implementation of hook_field_formatter_info().
 */
function feedfield_field_formatter_info() {
  return array(
      'default' => array(
        'label' => t('Display Aggregator Block (default)'),
        'field types' => array('feedfield'),
        'multiple values' => CONTENT_HANDLE_CORE,
        ),
      );
}

/**
 * FAPI theme for an individual text elements.
 */
function theme_feedfield($element) {
  return $element['#children'];
}

/**
 * Theme function for default formatter.  This is just a quick and dirty way of
 * showing feeds with the existing aggregator_block method.
 */
function theme_feedfield_formatter_default($element) {
  $fid = 'feed-'.$element['#item']['value']['fid'];
  $show = aggregator_block('view', $fid);
  return $show['content'];
}


/////// "private" helper functions ////////

/**
 * Helper function to add values to the node when the node is loaded.  Gets the values from the
 * aggregator tables.
 *
 * @param $node_field Array of field items.
 */
function _add_values_on_load($node_field, $field) {
  foreach ($node_field as $key => $item) {
    $feed=aggregation_get_feed_by_id($node_field[$key]['fid']);
    $categories =
      aggregation_get_assigned_feed_categories($node_field[$key]['fid']);
    $values[] = array(
        'value' => array(
          'title'=> $feed['title'],
          'feed' => $feed['url'],
          'refresh' => $feed['refresh'],
          'categories' => $categories,
          'fid' => $feed['fid']
          )
        );
  }

  $additions = array($field['field_name'] => $values);

  return $additions;
}

/**
 * Helper function to set a feed's categories.
 *
 * @param $categories Array of category cid's the feed should be
 * associated with.
 * @param $fid The feed's fid that should be associated with the categories.
 */
function _set_categories($categories, $fid) {
  foreach ($categories as $cid => $set) {
    // convert categories from option list to value list
    if ($set) $feed_categories[]=$cid;
  }
  aggregation_set_feed_categories($fid, $feed_categories);
}

/**
 * Just removes all feed items of a feed, then deletes the feed.
 *
 * @param The feed that will be deleted.
 */
function _remove_feed($fid) {
  aggregation_remove_feed_items($fid);
  aggregation_delete_feed($fid);
}

function _feedfield_validate(&$item, $delta, $field, &$node) {
  if (!empty($item['feed'])) {                  
    // Validate the URL
    if(!valid_url($item['feed'], TRUE)) {
      form_set_error($field['field_name'] .']['. $delta. '][feed', t('Not a valid URL.'));
    } elseif ($existing_feed=aggregation_get_feed_by_url($item['feed'])) {
      //url's must be unique
      if ($item['fid']!=$existing_feed['fid']) {
        form_set_error($field['field_name'] .']['. $delta. '][feed', t('This feed already exists in the aggregator.'));          
      }
    } elseif (empty($field['force_title']) && $existing_feed=aggregation_get_feed_by_title($item['title'])){
      // Titles must be unique
      if ($item['fid']!=$existing_feed['fid']) {
        form_set_error($field['field_name'] .']['. $delta. '][title', t('A feed already exists with this title.'));          
      }            
    } elseif (!empty($field['force_title']) &&
              $existing_feed=aggregation_get_feed_by_title($node->title)){
      // Titles must be unique
      if ($item['fid']!=$existing_feed['fid']) {
        form_set_error('title', t('A feed already exists with this title.'));          
      }            
    }       

  }
}

